import os
from frames2vid import vid2frames

folder_orig_video = \
    'C:/Data/Guillermo/Projects/R&D/2019/DC2/DataPrivacy/data/ADAS_routes'
file_name = 'Office_Workshop_DC2_Route.mp4'
path_to_video = os.path.join(folder_orig_video, file_name)

folder_frames = os.path.join('.',
                             'test/output')

file_output = ''.join(file_name.split('.')[:-1]) + '_out.mp4'

if not os.path.exists(folder_frames):
    os.makedirs(folder_frames)

vid2frames(path_to_video, folder_frames, prefix_fr='fr_', format_fn='%06d')

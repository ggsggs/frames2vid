import os
from frames2vid import get_fps_from_video, frames2vid


folder_orig_video = \
    '../../data/ADAS'
file_name = 'Office_Workshop_DC2_Route.mp4'
path_to_video = os.path.join(folder_orig_video, file_name)

folder_frames = os.path.join("/data/home/ggs/Documents/repos/CenterNet/exp/"
	"ctdet/demo_write_140/frames/")
                             
suffix_search = '.png'
folder_output = os.path.join(folder_frames, '..')

file_output = ''.join(file_name.split('.')[:-1]) + '_out.mp4'
path_to_video_out = os.path.join(folder_output, file_output)

images = [
    os.path.join(folder_frames, img) for img in os.listdir(folder_frames)
    if img.endswith(suffix_search)]

#images.sort(key=lambda x: x.replace('lp_', ''))
images.sort()

fps = get_fps_from_video(path_to_video)

frames2vid(images, path_to_video_out, fps, 'mp4v')

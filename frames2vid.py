import cv2
import numpy as np
import os.path
import sys


def get_fps_from_video(path_to_video):
    vidcap = cv2.VideoCapture(path_to_video)

    """ extract fps property mainly from:
    https://www.learnopencv.com/how-to-find-frame-rate-or-frames-per-second-fps-in-opencv-python-cpp/"""  # noqa

    # Find OpenCV version
    (major_ver, _, _) = (cv2.__version__).split('.')
    print(f'OpenCV version {major_ver}')

    if int(major_ver) < 3:
        fps = vidcap.get(cv2.cv.CV_CAP_PROP_FPS)
        print("Frames per second using video"
              ".get(cv2.cv.CV_CAP_PROP_FPS): {0}".format(fps))
    else:
        fps = vidcap.get(cv2.CAP_PROP_FPS)
        print(
            "Frames per second using video"
            ".get(cv2.CAP_PROP_FPS) : {0}".format(fps))

    vidcap.release()

    return fps


def frames2vid(ordered_list_fr_filenames, output_file, fps, fourcc='X264'):
    assert len(ordered_list_fr_filenames) > 0
    print('Accessing first file:\n\t{}'.format(ordered_list_fr_filenames[0]))
    fr = cv2.imread(ordered_list_fr_filenames[0])
    h, w, c = fr.shape

    print("Asuming all frames are {0}x{1}x{2}".format(h, w, c))

    vid = cv2.VideoWriter(
        output_file, cv2.VideoWriter_fourcc(*fourcc), fps, (w, h))

    print('Starting writing to file {}:'.format(output_file))
    for i, fr in enumerate(ordered_list_fr_filenames):
        vid.write(cv2.imread(fr))
        print('Image {}/{} written.'.format(
            i, len(ordered_list_fr_filenames)),
            end='\r')
        sys.stdout.flush()

    cv2.destroyAllWindows()
    vid.release()
    print(f'Completed! File written to :\n {output_file}')


def vid2frames(path_to_vid, path_to_fr_folder, prefix_fr='fr_',
               format_fn='%06d'):
    """ Splits a video into multiple frames """
    print('Reading file {}'.format(path_to_vid))
    vidcap = cv2.VideoCapture(path_to_vid)
    print(('Example format to be used: ' + prefix_fr + format_fn + '.png') % 1)
    print('Frames will be written to {}'.format(path_to_fr_folder))

    flag, img = vidcap.read()
    cnt = 0
    while flag:
        cv2.imwrite(os.path.join(
            path_to_fr_folder, (prefix_fr+format_fn+'.png') % cnt), img)
        flag, img = vidcap.read()
        cnt = cnt + 1
        print('Written {} files.'.format(cnt), end='\r')
        sys.stdout.flush()
    vidcap.release()

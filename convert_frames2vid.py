import os
import sys
import argparse
from frames2vid import get_fps_from_video, frames2vid

parser = argparse.ArgumentParser()
parser.add_argument('--fps_from_file', default='',
                    help='File to be used to extract fps from.')
parser.add_argument(
    '--folder_frames', default='',
    help='Folder that contains frames that will form the video')
parser.add_argument(
    '--suffix_search', default='.jpg',
    help='Suffix used to filter frames. ".jpg" or ".png", for instance.')
parser.add_argument(
    '--file_output', default='',
    help='Filename to be used as output, must contain video wrapper extension'
         '("mp4", etc.)')
parser.add_argument('--fourcc', default='mp4v',
                    help='String for codec selection. Default is "mp4v"')

if __name__ == '__main__':
    args = parser.parse_args()
    path_args = ['fps_from_file', 'folder_frames', 'file_output']
    for arg in path_args:
        if getattr(args, arg) == '':
            raise ValueError(f"Argument <--{arg}> has not been provided.")
    for arg in path_args[:2]:
        if not os.path.exists(getattr(args, arg)):
            raise ValueError(f"Argument <--{arg} {getattr(args,arg)}> is not"
                             "a valid path.")

images = [
    os.path.join(args.folder_frames, img)
    for img in os.listdir(args.folder_frames)
    if img.endswith(args.suffix_search)]

images.sort()

fps = get_fps_from_video(args.fps_from_file)
if fps <= 1e-3:
    raise ValueError('FPS of output video cannot be zero. Check'
                     '--fps_from_file argument.')
frames2vid(images, args.file_output, fps, args.fourcc)
